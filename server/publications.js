const PAGE_SIZE = 10;

Meteor.publish('data', function(page, options) {
  check(page, Match.Integer);
  options = options || {};
  check(options, Match.Maybe(Object));
  check(options.orderBy, Match.Maybe(String));
  check(options.order, Match.Maybe(Match.Integer));
  return GeneratedData.find({}, {
    skip: PAGE_SIZE * (page - 1),
    limit: PAGE_SIZE,
    sort: {
      ...options,
    },
  });
});

GeneratedData.allow({
  insert() {
    return false;
  },
  update() {
    return false;
  },
  remove() {
    return false;
  },
});
