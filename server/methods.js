import Jabber from 'jabber';
import { Random } from 'meteor/random';

const jabber = new Jabber();
const rawGeneratedData = GeneratedData.rawCollection();

Meteor.methods({
  async reset() {
    const dropData = Meteor.wrapAsync(rawGeneratedData.drop, rawGeneratedData);
    await dropData();
  },
  generateData() {
    const dataArray = [];

    for (let i = 0; i < 10000; i++) {
      // Since we are using rawCollection, Meteor executes this directly with database.
      // So instead of string ids rawCollection.insertMany uses Mongodb ObjectID by default
      // To stick with meteor convention using a random id here.
      const randomId = Random.id();

      const randomMonth = Math.floor(Math.random() * 12);
      const date = new Date();
      date.setMonth(randomMonth);

      dataArray.push({
        _id: randomId,
        field1: jabber.createWord(6),
        field2: jabber.createWord(6),
        field3: jabber.createWord(6),
        field4: jabber.createWord(6),
        field5: jabber.createWord(6),
        field6: jabber.createWord(6),
        field7: jabber.createWord(6),
        field8: jabber.createWord(6),
        field9: jabber.createWord(6),
        field10: jabber.createWord(6),
        activeIndicator: 'Y',
        effectiveDate: date,
        expiryDate: null,
      });
    }
    const insertData = Meteor.wrapAsync(rawGeneratedData.insertMany, rawGeneratedData);
    insertData(dataArray);
  },
  async getActiveIndicatorCount() {
    const aggregateQuery = Meteor.wrapAsync(rawGeneratedData.aggregate, rawGeneratedData);
    const pipeline = [{ $group: { _id: '$activeIndicator', count: { $sum: 1 } } }];
    const result = await aggregateQuery(pipeline).toArray();
    return result;
  },
  async getActiveIndicatorMonthlyCount() {
    const aggregateQuery = Meteor.wrapAsync(rawGeneratedData.aggregate, rawGeneratedData);
    const pipeline = [
      {
        $project: {
          month: { $month: '$effectiveDate' },
          year: { $year: '$effectiveDate' },
          activeIndicator: 1,
        },
      },
      {
        $group: {
          _id: { month: '$month', year: '$year', activeIndicator: '$activeIndicator' },
          count: { $sum: 1 },
        },
      },
      {
        $sort: {
          "_id.year": 1,
          "_id.month": 1
        }
      }
    ];
    const result = await aggregateQuery(pipeline).toArray();
    return result;
  },
  delete() {
    GeneratedData.update({ activeIndicator: 'Y' }, {
      $set: {
        activeIndicator: 'N',
        expiryDate: new Date(),
      },
    }, { multi: true });
  },
  expire() {
    GeneratedData.update({ expiryDate: null }, {
      $set: {
        expiryDate: new Date(),
      },
    }, { multi: true });
  },
});
