import { Meteor } from 'meteor/meteor';
import '../lib/collections';
import './publications';
import './methods';

const SORT_FIELDS = [
  "field1",
  "field2",
  "field3",
  "field4",
  "field5", 
  "field6",
  "field7",
  "field8",
  "field9",
  "field10",
  "activeIndicator",
  "effectiveDate",
  "expiryDate"
];

Meteor.startup(() => {
  // Creating indexes at startup could be constly depending on the db size. 
  // There are better ways to do this. https://docs.mongodb.com/manual/tutorial/build-indexes-on-replica-sets/

  SORT_FIELDS.forEach(field => {
    GeneratedData.rawCollection().createIndex({
      [field]: 1
    });
    GeneratedData.rawCollection().createIndex({
      [field]: -1
    });
  });
});
