import assert, { doesNotReject } from 'assert';

function removeAllEntities() {
  GeneratedData.remove({});
}

if(Meteor.isServer) {
  describe('server', function () {
    beforeEach(function setup(done) {
      removeAllEntities();
      done();
    });

    afterEach(function teardown(done) {
      removeAllEntities();
      done();
    });

    it('generateData', function (done) {
      Meteor.call('generateData');
      assert.equal(GeneratedData.find({}).count(), 10000);
      this.timeout(5000);
      done();
    });

    it('reset', function (done) {
      Meteor.call('generateData');
      Meteor.call('reset');
      assert.equal(GeneratedData.find({}).count(), 0);
      done();
    });

    it('getActiveIndicatorCount', function (done) {
      Meteor.call('generateData');
      const result = Meteor.call('getActiveIndicatorCount');
      assert.deepEqual(result, [{ _id: 'Y', count: 10000 }]);
      done();
    });
    it('getActiveIndicatorMonthlyCount', function (done) {
      Meteor.call('generateData');
      const result = Meteor.call('getActiveIndicatorMonthlyCount');
      assert.deepEqual(result.length, 12);
      done();
    });
    it('delete', function (done) {
      Meteor.call('generateData');
      const data = GeneratedData.findOne({});
      assert.equal(data.activeIndicator, "Y");
      
      Meteor.call('delete');
      const dataUpdated = GeneratedData.findOne({});
      assert.equal(dataUpdated.activeIndicator, "N");
      done();
    });
    it('expire', function (done) {
      Meteor.call('generateData');
      const data = GeneratedData.findOne({});
      assert.equal(data.expiryDate, null);
      
      Meteor.call('expire');
      const dataUpdated = GeneratedData.findOne({});
      assert.equal(dataUpdated.expiryDate instanceof Date, true);
      done();
    });
  });
}
