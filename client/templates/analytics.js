import Chart from 'chart.js';
import './analytics.html';

Template.analytics.onCreated(function () {
  this.pieChartData = new ReactiveVar([]);
  Meteor.call('getActiveIndicatorCount', (error, dataWithLabels = []) => {
    this.pieChartData.set(dataWithLabels);
  });
  this.lineChartData = new ReactiveVar([]);
  Meteor.call('getActiveIndicatorMonthlyCount', (error, data = []) => {
    this.lineChartData.set(data);
  });
});

function transformData(dataWithLabels) {
  const monthsObject = {};
  const yCount = {};
  const nCount = {};
  dataWithLabels.forEach((element) => {
    const month = `${element._id.year}-${element._id.month}`;
    monthsObject[month] = {
      month: element._id.month,
      year: element._id.year,
    };
    if (element._id.activeIndicator === 'N') {
      nCount[month] = element.count;
    } 
    if (element._id.activeIndicator === 'Y'){
      yCount[month] = element.count;
    }
  });
  const labels = Object.keys(monthsObject);

  return {
    labels,
    nCount,
    yCount
  };
}

Template.analytics.onRendered(function () {
  let pieChart;
  let lineChart;
  this.autorun(() => {
    // create tracker deps to resize
    Session.get('resize');
    const dataWithLabels = Template.instance().pieChartData.get();
    const data = dataWithLabels.map((element) => element.count);
    const labels = dataWithLabels.map((element) => element._id);

    // Update only, if window resize triggered
    if(pieChart) {
      pieChart.data.labels = labels;
      pieChart.data.datasets[0].data = data;
      pieChart.update();
      return;
    }

    pieChart = new Chart('pie-chart', {
      type: 'pie',
      data: {
        datasets: [{
          data,
          backgroundColor: [
            '#45b29d',
            '#ff9800',
          ],
        }],
        labels,
      },
      options: {
        responsive: true,
        title: {
          display: true,
          text: 'Active Indicator distribution summary',
        },
      },
    });
  });

  this.autorun(() => {
    // create tracker deps to resize
    Session.get('resize');

    const dataWithLabels = Template.instance().lineChartData.get();
    const { labels, yCount, nCount } = transformData(dataWithLabels);

    if(lineChart) {
      lineChart.data.labels = labels;
      lineChart.data.datasets[0].data = Object.values(yCount);
      lineChart.data.datasets[1].data = Object.values(nCount);
      lineChart.update();
      return;
    }
    
    lineChart = new Chart('line-chart', {
      type: 'line',
      data: {
        labels: labels,
        datasets: [{
          label: 'Y',
          backgroundColor: '#45b29d',
          borderColor: '#45b29d',
          fill: false,
          data: Object.values(yCount),
        }, {
          label: 'N',
          backgroundColor: '#ff9800',
          borderColor: '#ff9800',
          fill: false,
          data: Object.values(nCount),
        }],
      },
      options: {
        responsive: true,
        title: {
          display: true,
          text: 'Monthly Active Indicator distribution',
        },
        scales: {
          xAxes: [{
            display: true,
          }],
          yAxes: [{
            display: true,
          }],
        },
      },
    });
  });
});
