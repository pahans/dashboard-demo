import './list.html';
import moment from 'moment';

Template.list.onCreated(function () {

});

Template.list.helpers({
  getList() {
    return GeneratedData.find({});
  },
  firstPage() {
    const page = parseInt(Router.current().params._page) || 1;
    return page === 1;
  },
  formatedDate(date) {
    if (!date) {
      return 'null';
    }
    return moment(date).format('L');
  },
  getPageNumber(offset) {
    let page = parseInt(Router.current().params._page);
    page = page || 1;
    return page + offset;
  },
  getSlug(offset) {
    let page = parseInt(Router.current().params._page);
    page = page || 1;
    const params = Router.current().params.query;
    const queryString = Object.keys(params).map((key) => `${key}=${params[key]}`).join('&');
    return `${page + offset}?${queryString}`;
  },
  getSortOrder(orderby) {
    if (orderby === Router.current().params.query.orderby) {
      if (Router.current().params.query.order === 'dsc') {
        return 'asc';
      }
      return 'dsc';
    }

    return 'asc';
  },
  routeContext() {
    return {
      _page: Router.current().params._page,
    };
  },
  getSortIcon(orderby) {
    if (orderby === Router.current().params.query.orderby) {
      if (Router.current().params.query.order === 'dsc') {
        return 'fa-caret-down';
      }
      return 'fa-caret-up';
    }

    return 'fa-sort';
  },
});
