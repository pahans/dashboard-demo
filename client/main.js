import { Template } from 'meteor/templating';
import './main.html';
import '../lib/collections';
import './templates';
import './layouts';
import './routes';

Session.set("resize", null); 
Meteor.startup(function () {
  window.addEventListener('resize', function(){
    Session.set("resize", new Date());
  });
});

Template.home.events({
  'click #btn-generate'(event, instance) {
    Meteor.call('generateData');
  },
  'click #btn-reset'(event, instance) {
    Meteor.call('reset');
  },
  'click #btn-delete'(event, instance) {
    Meteor.call('delete');
  },
  'click #btn-expire'(event, instance) {
    Meteor.call('expire');
  },
});

Template.home.onCreated(function generateOnCreated() {

});
