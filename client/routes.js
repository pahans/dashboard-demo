Router.configure({
  layoutTemplate: 'default'
});

Router.route('/', function () {
  this.render('home');
});

Router.route('/analytics', function () {
  this.render('analytics');
});

Router.route('/list/:_page?', function () {
  const page = parseInt(this.params._page || 1);
  const { query } = this.params;
  let subsOptions;
  if (query.orderby) {
    subsOptions = { [query.orderby]: query.order === 'dsc' ? -1 : 1 };
  }
  this.subscribe('data', page, subsOptions).wait();
  if (this.ready()) {
    this.render('list', {
      _page: Router.current().params._page,
    });
  } else {
    this.render('Loading');
  }
}, {
  name: 'list',
});
