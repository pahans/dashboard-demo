# Dashboard demo

## How to run
`meteor run`
## Deployed URL
[dashboard-demo.meteorapp.com](dashboard-demo.meteorapp.com)
## Process Followed
Used mongodb bulk insert directly to insert data. (better performance)
Added indexing to improve sort performance in /list